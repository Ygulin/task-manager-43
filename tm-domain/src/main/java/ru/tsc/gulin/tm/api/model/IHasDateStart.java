package ru.tsc.gulin.tm.api.model;

import org.jetbrains.annotations.Nullable;

import java.util.Date;

public interface IHasDateStart {

    @Nullable
    Date getDateStart();

    void setDateStart(@Nullable Date dateStart);

}
